package es.ilerna.accesoadatos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InicioPrograma {

	private static final String SEPARADOR = "****************************************";

	private static List<Alumno> alumnos;
	private static int identificador;
	private static Scanner scanner;
	
	public static void main(String[] args) throws Exception {
		alumnos = new ArrayList<>();
		identificador = 0;
		boolean isNotExit = true;
		String seleccion = "0";

		do {
			imprimirMenuPrincipal();
			scanner = new Scanner(System.in);
			seleccion = scanner.nextLine();

			try {
				switch(seleccion) {
				case "1":// Introducir un alumno
					realizarAltaDeAlumnos();
					break;
				case "2":// Crear el fichero con nombre alumnos.dat
					creacionDelFichero();
					break;
				case "3":// Modificar el n�mero de asignaturas matriculadas
					modificacionDelNumeroDeAsign();
					break;
				case "4":// Eliminar un alumno
					eliminacionDeAlumnos();
					break;
				case "5":// Mostrar datos
					mostrarLaInformacionDeLosAlumnos();
					break;
				case "6":// Salir
					isNotExit = false;
					scanner.close();
					break;
				}
			} catch (Exception e) {
				imprimirPorPantalla(e.getMessage());
			}
		} while (isNotExit);

		salirDeAplicacion();
	}

	// Introducir un alumno
	private static Alumno altaAlumno(final int identificador, final String nombre, final String apellidos, int numeroAsignaturas) {
		return new Alumno(identificador, nombre, apellidos, numeroAsignaturas);
	}

	// Crear el fichero con nombre alumnos.dat
	@SuppressWarnings("null")
	private static void crearFichero(final String nombrefichero, final List<String> infoAlumnos) throws IOException {
		FileWriter fw = null;

		try {
			String fileDefault = "alumnos.dat";
			if (null != nombrefichero && !nombrefichero.isEmpty()) {
				fileDefault = nombrefichero;
			}
			fw = new FileWriter(new File(fileDefault));
			for (String fila : infoAlumnos) {
				fw.write(fila);
			}
			fw.flush();
		} catch (IOException e) {
			imprimirPorPantalla(e.getMessage());

			if (null == fw) {
				fw.close();
			}
		}
	}
	
	// Modificar el n�mero de asignaturas matriculadas
	// pasar el identificador del alumno, buscar el alumno en 
	// la lista de alumnos y modificar el numero de asign. de dicho alumno
	private static void modificarNumeroAsignaturas(final String idAlumno, final String numeroAsignaturas) {
		for (Alumno a : alumnos) {
			if (Integer.valueOf(idAlumno) == a.getIdentificador()) {
				final Alumno auxAlumno = a;
				final int index = alumnos.indexOf(a);
				auxAlumno.setNumeroAsignaturas(Integer.valueOf(numeroAsignaturas));
				alumnos.remove(a);
				alumnos.add(index, auxAlumno);
				break;
			}
		}
	}

	// Eliminar un alumno
	// pasar el identificador del alumno y eliminarlo de la lista de alumnos.
	private static void bajaAlumno(final String idAlumno) {
		for (Alumno a : alumnos) {
			if (Integer.valueOf(idAlumno) == a.getIdentificador()) {
				alumnos.remove(a);
				break;
			}
		}
	}

	// Mostrar datos (Imprimir todos los alumnos y su info por pantalla)
	private static void mostrarLaInformacionDeLosAlumnos() {
		int count = 0;

		for (Alumno a : alumnos) {
			if (count == 0) {
				imprimirPorPantalla(SEPARADOR);
			}

			imprimirPorPantalla(
					String.valueOf("Identificador: " + a.getIdentificador()),
					"Nombre alumno: " + a.getNombreAlumno(),
					String.valueOf("N�mero asignaturas: " + a.getNumeroAsignaturas()),
					SEPARADOR
			);
			++count;
		}
	}

	// Salir(Salir del menu)
	private static void salirDeAplicacion() {
		System.exit(0);
	}
	
	private static void imprimirPorPantalla(Object... object) {
		for (Object o : object) {
			System.out.println(o);
		}
	}

	private static void imprimirMenuPrincipal() {
		imprimirPorPantalla(
				"******* Ejercicio 6 - UF1 - PAC1 *******",
				"\n",
				"Introduce un n�mero para seleccionar una opci�n:",
				"1. Introducir un alumno",
				"2. Crear un fichero. (Por defecto con nombre \"alumnos.dat\")",
				"3. Modificar el n�mero de asignaturas matriculadas",
				"4. Eliminar un alumno",
				"5. Mostrar datos",
				"6. Salir"
		);
	}

	private static void realizarAltaDeAlumnos() throws Exception {
		imprimirPorPantalla("Introduce Nombre: ");
		final String nombre = scanner.nextLine();
		comprobarNombre(nombre);
		
		imprimirPorPantalla("Introduce Apellidos: ");
		final String  apellidos = scanner.nextLine();
		comprobarApellidos(apellidos);

		imprimirPorPantalla("Introduce N�mero de Asignaturas: ");
		final String strNumAsignaturas = scanner.next();
		comprobarNumAsignaturas(strNumAsignaturas);
		int numAsignaturas = 0;

		if (strNumAsignaturas != null && !strNumAsignaturas.isEmpty()) {
			numAsignaturas = Integer.valueOf(strNumAsignaturas);
		}

		alumnos.add(altaAlumno(++identificador, nombre, apellidos, numAsignaturas));
	}

	private static void comprobarCampo(final String valorCampo, final String nombreCampo) throws Exception {
		if (null == valorCampo || valorCampo.equals("")) {
			throw new Exception("[ERROR]: El par�metro [" + nombreCampo + ":{" + valorCampo
					+ "}] introducido es inv�lido.");
		}
	}

	private static void comprobarIdAlumno(final String idAlumno) throws Exception {
		comprobarCampo(idAlumno, "idAlumno");
	}
	
	private static void comprobarNombre(final String nombre) throws Exception {
		comprobarCampo(nombre, "nombre");
	}

	private static void comprobarApellidos(final String apellidos) throws Exception {
		comprobarCampo(apellidos, "apellidos");
	}

	private static void comprobarNumAsignaturas(final String numAsignaturas) throws Exception {
		// Tope maximo de asignaturas en 19
		if (null == numAsignaturas || numAsignaturas.equals("") || !numAsignaturas.matches("([0-9]|[0-1][0-9])")) {
			throw new Exception("[ERROR]: El par�metro [numAsignaturas:{" + numAsignaturas
					+ "}] introducido es inv�lido.");
		}
	}
	
	private static void creacionDelFichero() throws IOException {
		final List<String> list = new ArrayList<>();
		int count = 0;

		for (Alumno a : alumnos) {
			if (count == 0) {
				list.add(SEPARADOR + "\n");
			}

			list.add("Identificador: " + a.getIdentificador() + "\n");
			list.add("Nombre alumno: " + a.getNombreAlumno() + "\n");
			list.add("N�mero asignaturas: " + a.getNumeroAsignaturas() + "\n");
			list.add(SEPARADOR + "\n");
			++count;
		}
	
		if (null != alumnos && !alumnos.isEmpty()) { 
			crearFichero(null, list);
			imprimirPorPantalla("Fichero creado con exito.");
		} else {
			imprimirPorPantalla("No existen datos que guarda.");
		}
	}

	private static void modificacionDelNumeroDeAsign() throws Exception {
		if (null == alumnos || alumnos.isEmpty()) {
			return;
		}

		imprimirPorPantalla("Introducir c�digo Id del alumnio:");
		final String idAlumno = scanner.nextLine();
		comprobarIdAlumno(idAlumno);

		imprimirPorPantalla("Introducir el n�mero de asignaturas:");
		final String numeroAsignaturas = scanner.nextLine();
		comprobarNumAsignaturas(numeroAsignaturas);
		modificarNumeroAsignaturas(idAlumno, numeroAsignaturas);
	}
	
	private static void eliminacionDeAlumnos() throws Exception {
		if (null == alumnos || alumnos.isEmpty()) {
			return;
		}

		imprimirPorPantalla("Introducir c�digo Id del alumnio:");
		final String idAlumno = scanner.nextLine();
		comprobarIdAlumno(idAlumno);
		bajaAlumno(idAlumno);
	}

}

package es.ilerna.accesoadatos;

public class Alumno extends Persona {

	private static final long serialVersionUID = 1L;

	private int identificador;

	private String nombreAlumno;

	private int numeroAsignaturas;

	public Alumno(final int identificador, final String nombre, final String apellidos, int numeroAsignaturas) {
		this.identificador = identificador;
		this.nombreAlumno = concatNombreApellidos(nombre, apellidos);
		this.numeroAsignaturas = numeroAsignaturas;
	}

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getNombreAlumno() {
		return nombreAlumno;
	}

	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}

	public int getNumeroAsignaturas() {
		return numeroAsignaturas;
	}

	public void setNumeroAsignaturas(int numeroAsignaturas) {
		this.numeroAsignaturas = numeroAsignaturas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String concatNombreApellidos(final String nombre, final String apellidos) {
		return 	nombreAlumno = nombre + " " + apellidos;
	}

	public void resetIdentificador() {
		this.identificador = 0;
	}
	
	@Override
	public String toString() {
		return "Alumno [identificador=" + identificador + ", nombreAlumno=" + nombreAlumno + ", numeroAsignaturas="
				+ numeroAsignaturas + "]";
	}

}

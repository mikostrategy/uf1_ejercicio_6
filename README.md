EJERCICIO PRÁCTICO - PARA EL CICLO FORMATIVO DE GRADO SUPERIOR DAM 


ENUNCIADO

Realiza un programa en Java que cree un fichero binario para guardar la
información sobre los alumnos de Ilerna Online. Es necesario conocer el
identificador, el nombre del alumno y el número de asignaturas escogidas. 

El programa debe tener un menú que permita: 
-	Crear el fichero con nombre alumnos.dat 
-	Introducir un alumno 
-	Modificar el número de asignaturas matriculadas 
-	Eliminar un alumno 
-	Mostrar datos 
-	Salir 

Utiliza la parte de excepciones para realizar todas las validaciones de
datos que consideres oportunas.
